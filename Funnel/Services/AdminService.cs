﻿using Microsoft.EntityFrameworkCore;
using Funnel.DataBaseContext;
using Funnel.Interface;
using Funnel.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Services
{
    public class AdminService : IAdminService
    {
        private ApplicationDbContext _context;
        public AdminService(ApplicationDbContext context)
        {
            _context = context;
        }
        //This is Role Details
        public IEnumerable<UserInfoModel> GetAdminUserService()
        {
            return _context.Tbl_FunnelUserInfo;
        }
        public IEnumerable<RolesModel> GetRoleService()
        {
            return _context.Tbl_FunnelAdminRoles;
        }
        public IEnumerable<RolesModel> GetActiveRoleService()
        {
            return _context.Tbl_FunnelAdminRoles.Where(x => x.Status == "Active");
        }
        public RolesModel AddRoleService(RolesModel userRole)
        {
            if (CheckRoleAlreayExit(userRole) == null)
            {
                _context.Tbl_FunnelAdminRoles.Add(userRole);
                _context.SaveChanges();
            }
            return userRole;
        }
        public RolesModel EditRoleService(RolesModel userRole)
        {
            var userDetails = _context.Tbl_FunnelAdminRoles.Where(x => x.RoleID == userRole.RoleID).FirstOrDefault();
            if (userDetails != null)
            {
                userDetails.RoleName = userRole.RoleName;
                userDetails.Roles_Resp = userRole.Roles_Resp;
                userDetails.Status = userRole.Status;
                _context.Tbl_FunnelAdminRoles.Update(userDetails);
                _context.SaveChanges();
                return userDetails;
            }
            return null;
        }
        public RolesModel DeleteRoleService(RolesModel userRole)
        {
            var userRoleDetails = _context.Tbl_FunnelAdminRoles.Where(x => x.RoleID == userRole.RoleID).FirstOrDefault();
            if (userRoleDetails != null)
            {
                userRoleDetails.Status = userRole.Status;
                _context.Tbl_FunnelAdminRoles.Update(userRoleDetails);
                _context.SaveChanges();
                return userRoleDetails;
            }
            return null;
        }

        
        //This is User Details
        public IQueryable<UserInfoModel> GetUserInfoService()
        {
            var users = _context.Tbl_FunnelUserInfo;
            return users;
        }
        public IEnumerable<UserInfoModel> GetActiveUserService()
        {
            return _context.Tbl_FunnelUserInfo.Where(x => x.Status == "Active");
        }
        public UserInfoModel UserAddInfoService(UserInfoModel userDetails)
        {
            if (CheckUserAlreayExit(userDetails) == null)
            {
                _context.Tbl_FunnelUserInfo.Add(userDetails);
                _context.SaveChanges();
                return userDetails;
            }
            return null;
        }
        public UserInfoModel EditUserInfoService(UserInfoModel user)
        {
            var userDetails = _context.Tbl_FunnelUserInfo.Where(x => x.SNo == user.SNo).FirstOrDefault();
            if (userDetails != null)
            {
                userDetails.UserID = user.UserID;
                userDetails.UserName = user.UserName;
                userDetails.UserRoleName = user.UserRoleName;
                userDetails.UserEmailId = user.UserEmailId;
                userDetails.ModifiedBy = user.ModifiedBy;
                userDetails.Status = user.Status;
                _context.Tbl_FunnelUserInfo.Update(userDetails);
                _context.SaveChanges();
                return userDetails;
            }
            return null;
        }
        public UserInfoModel DeleteUserDetails(UserInfoModel user)
        {
            var userDetails = _context.Tbl_FunnelUserInfo.Where(x => x.SNo == user.SNo).FirstOrDefault();
            if (userDetails != null)
            {
                userDetails.Status = user.Status;
                userDetails.ModifiedBy = user.ModifiedBy;
                _context.Tbl_FunnelUserInfo.Update(userDetails);
                _context.SaveChanges();
                return userDetails;
            }
            return null;
        }
        //Menu
        
        //Depend Function
        private UserInfoModel CheckUserAlreayExit(UserInfoModel userDetails)
        {
            var addFriendDetails = _context.Tbl_FunnelUserInfo.Where(x => x.UserEmailId == userDetails.UserEmailId).FirstOrDefault();
            return addFriendDetails;
        }
        private RolesModel CheckRoleAlreayExit(RolesModel userRole)
        {
            var exitRoleDetails = _context.Tbl_FunnelAdminRoles.Where(x => x.RoleID == userRole.RoleID).FirstOrDefault();
            return exitRoleDetails;
        }             

    }
}
