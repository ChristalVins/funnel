﻿using Funnel.DataBaseContext;
using Funnel.Interface;
using Funnel.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Services
{
    public class EmployeeService : IEmployeeService 
    {
        private ApplicationDbContext _context;
        public EmployeeService(ApplicationDbContext context)
        {
            _context = context;
        }
        public IQueryable GetEmployeeHistroyService(EmployeeHistroyModel emp)
        {
            return _context.Tbl_EmployeeHistroy.FromSqlRaw("SP_Funnel_EMP_History_PowerApps {0}", emp.Employee_ID);
        }
        public IQueryable GetScopingHistroyService(ScopingHistroyModel scop)
        {
            return _context.Tbl_ScopingHistroy.FromSqlRaw("SP_UNION_SCOPING_POWERAPPS_Search {0}", scop.Employee_ID);
        }
    }
}
