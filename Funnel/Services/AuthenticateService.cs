﻿using Funnel.DataBaseContext;
using Funnel.Interface;
using Funnel.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Services
{
    public class AuthenticateService : IAuthenticateService
    {
        public IConfiguration Configuration { get; }
        private ApplicationDbContext _context;
        public AuthenticateService(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        public UserInfoModel LoginService(UserInfoModel UserDetails)
        {
            var users = _context.Tbl_FunnelUserInfo.Where(x => x.UserEmailId == UserDetails.UserEmailId && x.Status == "Active").FirstOrDefault();
            if (users != null)
            {
                return users;
            }
            else
            {
                return null;
            }
        }
    }
}
