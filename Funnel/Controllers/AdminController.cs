﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Funnel.Helpers;
using Funnel.Interface;
using Funnel.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Funnel.Controllers
{
    public class AdminController : Controller
    {
        private IServiceProvider _context;

        private readonly ILogger<AdminController> _logger;

        private IAdminService _adminService => _context.GetService(typeof(IAdminService)) as IAdminService;
        public AdminController(IServiceProvider context, ILogger<AdminController> logger)
        {
            this._context = context;
            _logger = logger;
        }
        [AllowAnonymous]
        [HttpGet("api/[controller]/userdetails")]
        public IActionResult GetAdminUserDetails()
        {
            try
            {
                var users = _adminService.GetAdminUserService();
                return Ok(users);
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "GetAdminUserDetails!");
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("api/[controller]/role")]
        public IActionResult GetRole()
        {
            try
            {
                var user = _adminService.GetRoleService();
                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });
                return Ok(user);
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "GetRole!");
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("api/[controller]/activerole")]
        public IActionResult GetActiveRole()
        {
            try
            {
                var user = _adminService.GetActiveRoleService();
                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });
                return Ok(user);
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "GetActiveRole!");
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpPost("api/[controller]/addrole")]
        public IActionResult AddRole([FromBody]RolesModel userRole)
        {
            try
            {
                if (userRole != null)
                {
                    _adminService.AddRoleService(userRole);
                    return Ok(userRole);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "AddRole!");
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/editrole")]
        public IActionResult EditRole([FromBody]RolesModel userRole)
        {
            try
            {
                if (userRole != null)
                {
                    _adminService.EditRoleService(userRole);
                    return Ok(userRole);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "EditRole!");
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/removerole")]
        public IActionResult RemoveRole([FromBody]RolesModel userRole)
        {
            try
            {
                if (userRole != null)
                {
                    var friends = _adminService.DeleteRoleService(userRole);
                    return Ok(friends);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "RemoveRole!");
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet("api/[controller]/user")]
        public IActionResult DisplayUserInfo()
        {
            try
            {
                var user = _adminService.GetUserInfoService();

                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                return Ok(user);
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "DisplayUserInfo!");
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpGet("api/[controller]/activeuser")]
        public IActionResult GetActiveUserInfo()
        {
            try
            {
                var user = _adminService.GetActiveUserService();

                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                return Ok(user);
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "GetActiveUserInfo!");
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/adduser")]
        public IActionResult AddUserInfo([FromBody]UserInfoModel userDetails)
        {
            try
            {
                if (userDetails != null)
                {
                    var user = _adminService.UserAddInfoService(userDetails);
                    if (user == null)
                    {
                        return Ok(user);
                    }
                    return Ok(user);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "AddUserInfo!");
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/edituser")]
        public IActionResult EditUserInfo([FromBody]UserInfoModel userDetails)
        {
            try
            {
                if (userDetails != null)
                {
                    _adminService.EditUserInfoService(userDetails);
                    return Ok(userDetails);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "EditUserInfo!");
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/deleteuser")]
        public IActionResult RemoveUser([FromBody]UserInfoModel UserDetails)
        {
            try
            {
                if (UserDetails != null)
                {
                    var friends = _adminService.DeleteUserDetails(UserDetails);
                    return Ok(friends);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "RemoveUser!");
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}