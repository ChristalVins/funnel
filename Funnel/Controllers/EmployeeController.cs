﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Funnel.Helpers;
using Funnel.Interface;
using Funnel.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Funnel.Controllers
{
    public class EmployeeController : Controller
    {
        private IServiceProvider _context;

        private readonly ILogger<EmployeeController> _logger;
        private IEmployeeService _empService => _context.GetService(typeof(IEmployeeService)) as IEmployeeService;
        public EmployeeController(IServiceProvider context, ILogger<EmployeeController> logger)
        {
            this._context = context;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/employeehistory")]
        public IActionResult GetEmployeeHistroy([FromBody]EmployeeHistroyModel emp)
        {
            try
            {
                if (emp != null)
                {
                    var employee = _empService.GetEmployeeHistroyService(emp);
                    return Ok(employee);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "GetEmployeeHistroy!");
                return BadRequest(new { message = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpPost("api/[controller]/scopinghistroy")]
        public IActionResult GetScopingHistroy([FromBody]ScopingHistroyModel scop)
        {
            try
            {
                if (scop != null)
                {
                    var scoping = _empService.GetScopingHistroyService(scop);
                    return Ok(scoping);
                }
                return NotFound();
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "GetScopingHistroy!");
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}