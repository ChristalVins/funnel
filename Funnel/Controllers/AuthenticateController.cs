﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Funnel.Helpers;
using Funnel.Interface;
using Funnel.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Funnel.Controllers
{
    public class AuthenticateController : Controller
    {
        private IServiceProvider _context;

        private readonly ILogger<AuthenticateController> _logger;
        private IAuthenticateService _authService => _context.GetService(typeof(IAuthenticateService)) as IAuthenticateService;

        public AuthenticateController(IServiceProvider context, ILogger<AuthenticateController> logger)
        {
            this._context = context;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("api/[controller]/login")]
        public IActionResult Authenticate([FromBody]UserInfoModel model)
        {

            try
            {
                if (model != null)
                {
                    var claimsIdentity = User.Identity as ClaimsIdentity;
                    var userId = claimsIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
                    var emailAddress = claimsIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                    var displayName = claimsIdentity.Claims.FirstOrDefault(c => c.Type == "DisplayName");
                    var userRole = claimsIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                    if (displayName != null)
                    {
                        model.UserName = displayName.Value;
                    }

                    if (emailAddress != null)
                    {
                        model.UserEmailId = emailAddress.Value;
                    }

                    if (userId != null)
                    {
                        model.UserID = userId.Value;
                    }

                    var user = _authService.LoginService(model);
                    return Ok(user);
                }
                else
                {
                    _logger.LogError(new Exception(), "Authenticate!");
                    return NotFound(model);
                }
            }
            catch (AppException ex)
            {
                _logger.LogError(new Exception(), "Authenticate!");
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}