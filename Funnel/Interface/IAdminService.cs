﻿using Funnel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Interface
{
    interface IAdminService
    {
        IEnumerable<UserInfoModel> GetAdminUserService();
        IEnumerable<RolesModel> GetRoleService();
        IEnumerable<RolesModel> GetActiveRoleService();
        RolesModel AddRoleService(RolesModel userRole);
        RolesModel EditRoleService(RolesModel userRole);
        RolesModel DeleteRoleService(RolesModel userRole);

        IQueryable<UserInfoModel> GetUserInfoService();
        IEnumerable<UserInfoModel> GetActiveUserService();
        UserInfoModel UserAddInfoService(UserInfoModel userDetails);
        UserInfoModel EditUserInfoService(UserInfoModel UserDetails);
        UserInfoModel DeleteUserDetails(UserInfoModel UserDetails);
        
    }

}
