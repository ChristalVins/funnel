﻿using Funnel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Interface
{
    interface IEmployeeService
    {
        IQueryable GetEmployeeHistroyService(EmployeeHistroyModel emp);
        IQueryable GetScopingHistroyService(ScopingHistroyModel scop);
    }
}
