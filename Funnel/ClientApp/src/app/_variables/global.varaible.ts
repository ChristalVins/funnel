export class GlobalConstants {
    public static activeStatus: any[] = ['Active', 'Inactive'];
    public static status: any[] = [
            {label: 'Active', value: 'Active'},
            {label: 'Inactive', value: 'Inactive'},
        ];
}