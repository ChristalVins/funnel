import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_auth/auth.guard';


const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        loadChildren: () =>        
        import('./home-dashboard/home-dashboard.module').then(m => m.HomeDashboardModule)
    },
    {
        path: 'authentication',
        loadChildren: () =>
        import('./authentication/authentication.module').then(m=>m.AuthenticationModule)     
    },     
    {
        path: '', redirectTo: 'authentication', pathMatch: 'full'
    },
    { path: '**', redirectTo: 'authentication' }    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }