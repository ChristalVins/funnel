import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import {MessageService} from 'primeng/api';

import * as XLSX from 'xlsx';

import { OpenSnackBar } from '../../_helper/open-snack-bar.helper';
import { EmployeeService } from '../../_service/employee.service';
import { EmployeeModel } from '../../_model/employee.model';
import { ScopingModel } from '../../_model/scoping.model';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.less']
})
export class DashboardViewComponent implements OnInit {
  addForm: FormGroup;  
  loading = false;
  employeeResult:EmployeeModel[] = [];
  scopingResult:ScopingModel[] = [];
  employeeCols: any[];
  scopingCols:any[];
  submitted = false;
  showExportExcel = true;

  constructor(private fb: FormBuilder, private employeeService:EmployeeService, private openSnackBar:OpenSnackBar) { 
    
  }

  ngOnInit() {
    /*this.addForm = this.fb.group({  
      'employee_ID' : new FormControl('', Validators.required),
    }); */

    this.addForm = this.fb.group({
      employee_ID: ['', Validators.required]
    });

    this.employeeColsDetails();
    this.scopingColsDetails();

    this.showExportExcel = true;
    
  }

  employeeColsDetails() {
    this.employeeCols = [
      { field: 'employee_ID', header: 'Employee ID' },  
      { field: 'report_Date', header: 'Report Year/Month' },    
      { field: 'function_Details', header: 'Function' },      
      { field: 'job_Family_Group', header: 'Job Family Group' },      
      { field: 'job_Family', header: 'Job Family' },
      { field: 'job_Profile', header: 'Job Profile' },
      { field: 'job_Title', header: 'Job Title' },      
      { field: 'location_Address_Country', header: 'Location Address - Country' },
      { field: 'location_Address_City', header: 'Location Address - City' },      
      { field: 'location', header: 'Location' },      
      { field: 'cF_Location_Hierarchy_Geographical', header: 'CF_Location Hierarchy Geographical' },      
      { field: 'sO_Name', header: 'SO Name' },
      { field: 'sO_Level_Two', header: 'SO Level 2' },
      { field: 'sO_Level_Three', header: 'SO Level 3' },
      { field: 'sO_Level_Four', header: 'SO Level 4' },
      { field: 'compensation_Grade', header: 'Compensation Grade' },      
      { field: 'cost_Center_ID', header: 'Cost Center - ID' },
      { field: 'cost_Center_Name', header: 'Cost Center - Name' },    
    ];
  }
  scopingColsDetails() {
    this.scopingCols = [
      { field: 'employee_ID', header: 'Employee ID' },
      { field: 'stage', header: 'Stage' },
      { field: 'status', header: 'Status' },
      { field: 'file_Details', header: 'File' },    
      { field: 'date', header: 'Date' },    
      { field: 'remarks', header: 'Remarks' },
      //{ field: 'transition_Wave_Code', header: 'Transition Wave Code' },
      { field: 'transition_Journey_Code', header: 'Transition Journey Code' },
      
    ];
  }

  get f() { return this.addForm.controls; }

  onFormSubmit(event) {
    this.submitted = true;
    if (this.addForm.invalid) {
      return;
    }
    this.loading = true;
    this.employeeService.employeeHistoryService(this.addForm.value)
      .subscribe(
          data => {
            if(data) {
              this.employeeResult = data;
            }            
            this.loading = false;
            this.showExportExcel = false;
          },
          error => {
              this.loading = false;
              this.openSnackBar.openSnackBar('Error: Employee Details ', 'danger');
              this.showExportExcel = false;
          });
    this.employeeService.scopingHistoryService(this.addForm.value)
      .subscribe(
        data => {
          if(data) {
            this.scopingResult = data;
          }
          this.loading = false;
        },
        error => {
            this.loading = false;
            this.openSnackBar.openSnackBar('Error: Scoping History ', 'danger');
        });
  }

  exportExcel() {    
    debugger;
    import("xlsx").then(xlsx => {
        const worksheet = xlsx.utils.json_to_sheet(this.employeeResult);
        const worksheet2 = xlsx.utils.json_to_sheet(this.scopingResult);
        const workbook = { Sheets: { 'Employee History': worksheet, 'Scoping History':worksheet2 }, SheetNames: ['Employee History','Scoping History'] };
        const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "FunnelDetails");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }  


}
