import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MaterialModule } from '../app.material.module';
import { HomeDashboardRoutes } from './home-dashboard.routes';
import { TableModule, DropdownModule, EditorModule, MessageModule, InputTextModule, ButtonModule } from 'primeng';

import { HomeDashboardComponent } from './home-dashboard.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { HeaderComponent } from '../common/header/header.component';


@NgModule({
  declarations: [HomeDashboardComponent,DashboardViewComponent,HeaderComponent],
  imports: [
    CommonModule,
    HomeDashboardRoutes,
    ReactiveFormsModule, 
    FormsModule,
    FontAwesomeModule,
    MaterialModule,
    InputTextModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    EditorModule,
    MessageModule
  ]
})
export class HomeDashboardModule { }
