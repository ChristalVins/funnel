import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.less']
})
export class HomeDashboardComponent implements OnInit {

  collapsed: boolean;
  isActive:boolean;
  constructor() { }

  ngOnInit(): void {
    this.collapsed = true;
  }

  collapsedEvent(result) {
    this.collapsed = result;
  }

}
