import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeDashboardComponent } from './home-dashboard.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { AuthGuard } from '../_auth/auth.guard';

const homeRoutes: Routes = [
    {
      path: '',
      component:HomeDashboardComponent,
      children:[
        {
          path: '',
          component:DashboardViewComponent,
        },
        {
          path: 'user',
          loadChildren: () =>        
          import('../users/users.module').then(m => m.UsersModule),
          canActivate: [AuthGuard],
          data: { roles: ['Admin'] }
        },
        {
          path: 'role',
          loadChildren: () =>        
          import('../role/role.module').then(m => m.RoleModule),
          canActivate: [AuthGuard],
          data: { roles: ['Admin'] }
        }       
      ]
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(homeRoutes)],
    exports: [RouterModule]
  })
  
  export class HomeDashboardRoutes { }