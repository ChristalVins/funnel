import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatDialog } from '@angular/material';
import { OpenSnackBar } from '../_helper/open-snack-bar.helper';
import { first } from 'rxjs/operators';
import { DialogBoxUsersComponent } from './dialog-box-users/dialog-box-users.component';
import { AuthenticationService } from '../_service/authentication.service';
import { UserInfoModel } from '../_model/user-info.model';
import { UserService } from '../_service/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {

  loading: boolean;
  cols: any[];
  userResult:any;

  dataSource : UserInfoModel[];
  currentUser: UserInfoModel;
 
  @ViewChild(MatTable,{static:true}) table: MatTable<any>;

  constructor(public dialog: MatDialog, private userService:UserService, private openSnackBar:OpenSnackBar,
    private authenticationService: AuthenticationService,) { 
      this.currentUser = this.authenticationService.currentUserValue;     
  }

  ngOnInit() {
    this.cols = [
      //{ field: 'userID', header: 'User ID' },
      { field: 'userName', header: 'User Name' },
      { field: 'userRoleName', header: 'User Role Name' },
      { field: 'userEmailId', header: 'User Email ID' },
      { field: 'status', header: 'Status' },
      { field: 'action', header: 'Action' }
    ];
    this.displayUser();
  }

  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxUsersComponent, {
      width: '520px',
      data:obj
    });
 
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        if(result.event == 'Add'){
          this.addRowData(result.data);
        }else if(result.event == 'Update'){
          this.updateRowData(result.data);
        }else if(result.event == 'Delete'){
          this.deleteRowData(result.data);
        }
      }
    });
  }

  displayUser(){
    this.loading = true;
    this.userService.getAlldisplayUser()
      .pipe(first())
      .subscribe(data => {
        //this.userResult = data;
        if(data) {
          this.dataSource = data;
        }
        this.loading = false;
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
          this.loading = false;
        }
      );
  }
  
  addRowData(row_obj){
    row_obj.addedByID = this.currentUser.userEmailId;
    row_obj.modifiedBy ="";
    this.userService.addUser(row_obj)
      .pipe(first())
      .subscribe(
          data => {
            if(data !=null) {
              this.dataSource.push({
                sNo:data['sNo'],
                userID:row_obj.userID,
                userName:row_obj.userName,
                userRoleName:row_obj.userRoleName,
                userEmailId:row_obj.userEmailId,
                addedByID:row_obj.addedByID,
                modifiedBy:row_obj.modifiedBy,
                status:row_obj.status,
              });            
              this.openSnackBar.openSnackBar('User Details Added Sucessfully', 'primary');
              this.table.renderRows();
            }
            else {
              this.openSnackBar.openSnackBar('User Already Exists', 'danger');
            }
          },
          error => {
              this.openSnackBar.openSnackBar('Error', 'danger');
              console.error(error);
          });
  }
  
  updateRowData(row_obj){
    row_obj.modifiedBy =this.currentUser.userEmailId;
    this.userService.editUser(row_obj)
      .pipe(first())
      .subscribe(data => {
        this.dataSource = this.dataSource.filter((value,key)=>{
          if(value.sNo == row_obj.sNo){
            value.userID =row_obj.userID;
            value.userName =row_obj.userName;
            value.userRoleName = row_obj.userRoleName;
            value.userEmailId = row_obj.userEmailId;
            value.status =row_obj.status;
          }
          this.openSnackBar.openSnackBar('User Details Updated Sucessfully', 'primary');
          return true;
        });
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
        }
      );    
  }

  deleteRowData(row_obj){
    row_obj.modifiedBy =this.currentUser.userEmailId;
    row_obj.status = "Inactive";
    this.userService.deleteUserService(row_obj)
      .subscribe(data => {
        this.dataSource = this.dataSource.filter((value,key)=>{
          if(value.sNo == row_obj.sNo){
            value.status = row_obj.status;               
          }
          this.openSnackBar.openSnackBar('User Details Deleted Sucessfully', 'primary');
          return true;
        });
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
        }); 
  }

}
