import { Component, OnInit, Optional, Inject } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { first } from 'rxjs/operators';

import { UserInfoModel } from '../../_model/user-info.model';
import { AuthenticationService } from '../../_service/authentication.service';
import { OpenSnackBar } from '../../_helper/open-snack-bar.helper';
import { GlobalConstants } from '../../_variables/global.varaible';
import { RoleService } from '../../_service/role.service';

@Component({
  selector: 'app-dialog-box-users',
  templateUrl: './dialog-box-users.component.html',
  styleUrls: ['./dialog-box-users.component.less']
})
export class DialogBoxUsersComponent implements OnInit {

  addForm: FormGroup;  
  currentUser: UserInfoModel;
  action:string;
  local_data:any;
  role_level:any;
  hub_level:any;
  activeStatus: string[];
  emailPattern =".+@philips.com"

  public userModel: UserInfoModel;
 
  constructor(
    public dialogRef: MatDialogRef<DialogBoxUsersComponent>,private authenticationService: AuthenticationService,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data,private fb: FormBuilder,private roleService:RoleService,
    private openSnackBar:OpenSnackBar,) {    
    this.currentUser = this.authenticationService.currentUserValue;     
    this.local_data = {...data};   
    this.action = this.local_data.action;
  }
    
  ngOnInit(){
    this.activeStatus = GlobalConstants.activeStatus;
    this.addForm = this.fb.group({ 
      sNo:this.data.sNo ? this.data.sNo :Number,
      userID: [this.data.userID ? this.data.userID : '',],
      userName: [this.data.userName ? this.data.userName : '',Validators.required],
      userRoleName: [this.data.userRoleName ? this.data.userRoleName : '',Validators.required],
      userEmailId: [this.data.userEmailId ? this.data.userEmailId : '',[Validators.required,Validators.pattern(this.emailPattern)]],
      status: [this.data.status ? this.data.status : '',Validators.required]
    })   
    this.displayRole();
  }

  displayRole(){
    this.roleService.getActiveRoleService()
      .pipe(first())
      .subscribe(data => {
        if(data) {
          if(this.currentUser) {
            this.role_level = data;
          } 
        }               
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
        }
      );
  }


  doAction(){    
    if(this.addForm.value){
      this.dialogRef.close({event:this.action,data:this.addForm.value});
    }    
  }
 
  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }
}
