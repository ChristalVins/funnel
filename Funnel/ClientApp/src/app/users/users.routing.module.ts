import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { UsersComponent } from "./users.component";


export const UsersRoutes: Route[] = [
    {
      path: '',
      component: UsersComponent      
    }
];

@NgModule({
  imports: [RouterModule.forChild(UsersRoutes)],
  exports: [RouterModule]
})

export class UsersRouteModule { }