import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule, DropdownModule, PaginatorModule, EditorModule } from 'primeng';
import { UsersRouteModule } from './users.routing.module';

import { MaterialModule } from '../app.material.module';
import { UsersComponent } from './users.component';
import { DialogBoxUsersComponent } from './dialog-box-users/dialog-box-users.component';

@NgModule({
  declarations: [UsersComponent,DialogBoxUsersComponent],
  imports: [
    CommonModule,
    UsersRouteModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    TableModule,
    DropdownModule,
    PaginatorModule,
    EditorModule,   
  ],
  entryComponents: [
    DialogBoxUsersComponent
  ]
})
export class UsersModule { }
