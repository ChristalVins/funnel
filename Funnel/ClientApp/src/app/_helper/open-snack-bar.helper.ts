import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { SnackbarComponent } from "../common/snackbar/snackbar.component";

@Injectable({
    providedIn: 'root'
})

export class OpenSnackBar {
  
    constructor(public snackBar: MatSnackBar) { }
  
    openSnackBar(message: string, panelClass: string) {
        this.snackBar.openFromComponent(SnackbarComponent, {
          data: message,
          panelClass: panelClass,
          duration: 10000,
          verticalPosition: 'top',
          horizontalPosition: 'end'
        });
    }
}