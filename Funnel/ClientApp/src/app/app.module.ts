import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { ProgressBarModule, CalendarModule } from 'primeng';
import { MaterialModule } from './app.material.module';

import { AppComponent } from './app.component';
import { MyLoaderComponent } from './common/my-loader/my-loader.component';
import { AppRoutingModule } from './app-routing.module';
import { SnackbarComponent } from './common/snackbar/snackbar.component';
import { LoaderInterceptor } from './_helper/loader-interceptor.service';
import { LoaderService } from './_service/loader.service';
import { GlobalErrorHandler } from './_helper/global-error-handler';


library.add(fas, far);

@NgModule({
  declarations: [
    AppComponent,
    SnackbarComponent,
    MyLoaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FontAwesomeModule,
    CalendarModule
  ],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true } ,
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
  ],
  entryComponents: [
    SnackbarComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
