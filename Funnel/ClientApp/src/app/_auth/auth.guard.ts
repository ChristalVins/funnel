import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "../_service/authentication.service";

@Injectable({
    providedIn: 'root'
})
  
export class AuthGuard implements CanActivate {

    constructor( private authService: AuthenticationService, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authService.currentUserValue;
        const currnetMenuUrl = this.authService.checkMenuAvailable(state.url);
        if (currentUser) {
           
            if (route.data.roles && route.data.roles.indexOf(currentUser.userRoleName) === -1) {
                // role not authorised so redirect to home page
                this.router.navigate(['/']);
                return false;
            }
            /*if(currnetMenuUrl !=null) {
                if(currnetMenuUrl == true && state.url !='/') {
                    this.router.navigate(['/']);
                    return true;
                }
            
            }
            else {
                return false;
            }*/
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/authentication'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}