import { Injectable, Inject } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { UserInfoModel } from "../_model/user-info.model";
import { environment } from "src/environments/environment";

@Injectable({ providedIn: 'root' })

export class AuthenticationService  {
    private _isLoggedIn: boolean;

    baseURL: string;    
    private currentUserSubject: BehaviorSubject<UserInfoModel>;    
    public currentUser: Observable<UserInfoModel>; 

    constructor(private _http: HttpClient) {
        this.baseURL = environment.appUrl;  
        this.currentUserSubject = new BehaviorSubject<UserInfoModel>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserInfoModel {
        return this.currentUserSubject.value;
    }

    public checkMenuAvailable(url) {
        let menuList = JSON.parse(localStorage.getItem('currentMenu'));
        if (url !=null && menuList != null) {
            let result = menuList.filter(x => x.urlPath == url);             
            if(result.length == 0) {
                return true;
            }
        }        
        return false;
    }

    login(userEmailId: string) {
        return this._http.post<any>(`${this.baseURL}api/authenticate/login`, { userEmailId })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user) {
                    //if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }    

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
    
    getLeftMenu(screen) {
        return this._http.post<any>(`${this.baseURL}api/admin/getmenudetails`,screen);
    }

    addUser(userDetails): Observable<UserInfoModel[]> {
        return this._http.post<UserInfoModel[]>(`${this.baseURL}api/admin/adduser`, userDetails);
    }

    getAlldisplayUser(): Observable<UserInfoModel[]>{
        return this._http.get<UserInfoModel[]>(`${this.baseURL}api/admin/displayuser`);
    }

    editUser(userDetails): Observable<UserInfoModel[]> {
        return this._http.post<UserInfoModel[]>(`${this.baseURL}api/admin/edituser`, userDetails);
    }

    removeUserService(userID) {
        return this._http.post(`${this.baseURL}api/admin/deleteuser`, userID);
    }
    getMenuAcessRole(userID){
        return this._http.post(`${this.baseURL}api/admin/deleteuser`, userID);
    }
}