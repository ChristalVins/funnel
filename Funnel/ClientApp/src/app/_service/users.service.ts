import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { UserInfoModel } from "../_model/user-info.model";

@Injectable({
    providedIn: 'root'
})

export class UserService {
    baseURL: string;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
        })
    };

    constructor(private _http: HttpClient) {
        this.baseURL = environment.appUrl;
    }

    getAlldisplayUser(): Observable<UserInfoModel[]>{
        return this._http.get<UserInfoModel[]>(`${this.baseURL}api/admin/user`);
    }

    getActiveUser(): Observable<UserInfoModel[]>{
        return this._http.get<UserInfoModel[]>(`${this.baseURL}api/admin/activeuser`);
    }
    
    addUser(userDetails): Observable<UserInfoModel[]> {
        return this._http.post<UserInfoModel[]>(`${this.baseURL}api/admin/adduser`, userDetails);
    }    

    editUser(userDetails): Observable<UserInfoModel[]> {
        return this._http.post<UserInfoModel[]>(`${this.baseURL}api/admin/edituser`, userDetails);
    }

    deleteUserService(userID) {
        return this._http.post(`${this.baseURL}api/admin/deleteuser`, userID);
    }
}