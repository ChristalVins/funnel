import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

import { RoleModel } from "../_model/role.model";

@Injectable({
    providedIn: 'root'
})

export class RoleService {
    baseURL: string;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
        })
    };

    constructor(private _http: HttpClient) {
        this.baseURL = environment.appUrl;
    }

    getRoleService() {
        return this._http.get<RoleModel[]>(`${this.baseURL}api/admin/role`);
    }

    getActiveRoleService() {
        return this._http.get<RoleModel[]>(`${this.baseURL}api/admin/activerole`);
    }

    addRoleService(userRole): Observable<RoleModel[]> {
        return this._http.post<RoleModel[]>(`${this.baseURL}api/admin/addrole`,userRole);
    }

    editRoleService(userRole): Observable<RoleModel[]> {
        return this._http.post<RoleModel[]>(`${this.baseURL}api/admin/editrole`,userRole);
    }

    deleteRoleService(userRole): Observable<RoleModel[]> {
        return this._http.post<RoleModel[]>(`${this.baseURL}api/admin/removerole`,userRole);
    }
}