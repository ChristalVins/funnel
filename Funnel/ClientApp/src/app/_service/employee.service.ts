import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

import { EmployeeModel } from "../_model/employee.model";
import { ScopingModel } from "../_model/scoping.model";

@Injectable({ providedIn: 'root' })

export class EmployeeService  {
    baseURL: string;

    constructor(private _http: HttpClient) {
        this.baseURL = environment.appUrl;
    }
    employeeHistoryService(emp) {
        return this._http.post<EmployeeModel[]>(`${this.baseURL}api/employee/employeehistory`,emp);
    }
    scopingHistoryService(scop) {
        return this._http.post<ScopingModel[]>(`${this.baseURL}api/employee/scopinghistroy`,scop);
    }
}