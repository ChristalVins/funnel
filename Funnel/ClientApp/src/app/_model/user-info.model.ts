export class UserInfoModel {
    sNo:number;
    userID:string;
    userName:string;
    userRoleName:string;
    userEmailId:string;    
    addedByID:string;
    modifiedBy:string;
    status:string;
}