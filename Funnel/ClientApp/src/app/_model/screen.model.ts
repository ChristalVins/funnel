export class ScreenModel {
    sId:number;
    screenName:string;
    subScreenName:string;
    urlPath:string;
    roleName:string;    
    status:string;
    screenOrder:number;
}