export class ScopingModel {
    employee_ID:string;
    stage:string;
    status:string;
    file_Details:string;
    date:Date;
    remarks:string;
    //transition_Wave_Code:string;
    transition_Journey_Code:string;
}