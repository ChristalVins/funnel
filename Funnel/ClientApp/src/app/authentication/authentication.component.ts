import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../_service/authentication.service';
import { OpenSnackBar } from '../_helper/open-snack-bar.helper';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.less']
})
export class AuthenticationComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  acessDenied:string;

  isLoggedIn = false;
  userName = '';

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, 
    private authenticationService: AuthenticationService,
    private openSnackBar:OpenSnackBar) {
     
      if (this.authenticationService.currentUserValue) { 
          this.router.navigate(['/']);
      }
  }

  ngOnInit() { 
    /*this.loginForm = this.formBuilder.group({
      userEmailId: ['', Validators.required]
    });*/
    
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    //this.displayHeader();
    //this.getRequest();
    //this.login();
    this.Ssologin();
  }

  // convenience getter for easy access to form fields
  //get f() { return this.loginForm.controls; }  

  Ssologin() {
    let emailId ="";
    this.authenticationService.login(emailId)
      .subscribe(
          data => {
            //console.log("Backend-header",data);
            if(data !=null) {              
              this.router.navigate([this.returnUrl]); 
            }
            else {
              this.acessDenied ="acess-denied"
            }
          },
          error => {
              this.loading = false;
              this.openSnackBar.openSnackBar('Error: Email ID is incorrect ', 'danger');
          });
  }

}
