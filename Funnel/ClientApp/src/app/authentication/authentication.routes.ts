import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthenticationComponent } from "./authentication.component";


const authRoutes: Routes = [
    {
      path: '',
      component:AuthenticationComponent,
    }
  ];
  
@NgModule({
    imports: [RouterModule.forChild(authRoutes)],
    exports: [RouterModule]
})

export class AuthenticationRoutingModule { }