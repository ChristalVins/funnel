import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { OpenSnackBar } from 'src/app/_helper/open-snack-bar.helper';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { UserInfoModel } from 'src/app/_model/user-info.model';
import { ScreenModel } from 'src/app/_model/screen.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isActive: boolean;
  collapsed: boolean;
  showMenu: string;
  pushRightClass: string;
  currentUser: UserInfoModel;
  menulist:any;

  @Output() collapsedEvent = new EventEmitter<boolean>();
  
  constructor(private authenticationService:AuthenticationService,
    private router: Router, private openSnackBar:OpenSnackBar) {
    this.currentUser = this.authenticationService.currentUserValue;
    //this.currentMenu = this.authenticationService.currentMenuValue;     
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);   
  } 

  ngOnInit() {
    this.isActive = true;
    this.collapsed = true;
    this.showMenu = '';
    this.pushRightClass = 'push-left'; 
  }
  
  get isAdmin() {
    return this.currentUser && this.currentUser.userRoleName === 'Admin';
  }
  
  toggleCollapsed(){
    this.collapsed = !this.collapsed;
    this.collapsedEvent.emit(this.collapsed);
  }

  logoutAction() {
    this.authenticationService.logout();
    this.router.navigate(['/authentication']);
  }

}
