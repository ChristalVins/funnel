import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { RoleComponent } from "./role.component";

export const RoleRoutes: Route[] = [
    {
      path: '',
      component: RoleComponent      
    }
];

@NgModule({
  imports: [RouterModule.forChild(RoleRoutes)],
  exports: [RouterModule]
})

export class RoleRouteModule { }