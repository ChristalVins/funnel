import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { MatTable, MatDialog } from '@angular/material';
import { RoleService } from '../_service/role.service';
import { OpenSnackBar } from '../_helper/open-snack-bar.helper';
import { RoleModel } from '../_model/role.model';
import { DialogBoxRoleComponent } from './dialog-box-role/dialog-box-role.component';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  displayedColumns: string[] = ['roleID', 'roleName','status','action'];
  dataSource : RoleModel[];
 
  @ViewChild(MatTable,{static:true}) table: MatTable<any>;

  constructor(public dialog: MatDialog, private roleService:RoleService, private openSnackBar:OpenSnackBar) { }

  ngOnInit() {
    this.displayRole();
  }

  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxRoleComponent, {
      width: '450px',
      data:obj
    });
 
    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        if(result.event == 'Add'){
          this.addRowData(result.data);
        }else if(result.event == 'Update'){
          this.updateRowData(result.data);
        }else if(result.event == 'Delete'){
          this.deleteRowData(result.data);
        }
      }
    });
  }

  addRowData(row_obj){
    this.roleService.addRoleService(row_obj)
      .pipe(first())
      .subscribe( data => {
        this.dataSource.push({
          roleID:data['roleID'],
          roleName:row_obj.roleName,
          roles_Resp:row_obj.roles_Resp,
          status:row_obj.status
        });
        this.table.renderRows();
        this.openSnackBar.openSnackBar('User Details Added Sucessfully', 'primary');
      },
      error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
      });    
  }

  displayRole(){
    this.roleService.getRoleService()
      .pipe(first())
      .subscribe(data => {
        this.dataSource = data;
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
        }
      );
  }
  
  updateRowData(row_obj){
    this.roleService.editRoleService(row_obj)
      .pipe(first())
      .subscribe(data => {
        this.dataSource = this.dataSource.filter((value,key)=>{
          if(value.roleID == row_obj.roleID){
            value.roleName =row_obj.roleName;
            value.roles_Resp = row_obj.roles_Resp;
            value.status =row_obj.status;
          }
          this.openSnackBar.openSnackBar('Role Details Updated Sucessfully', 'primary');
          return true;
        });
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
        console.error(error);
        }
      );    
  }

  deleteRowData(row_obj){
    row_obj.status = "Inactive";
    this.roleService.deleteRoleService(row_obj)
      .subscribe(data => {
        this.dataSource = this.dataSource.filter((value,key)=>{
          if(value.roleID == row_obj.roleID){
            value.status = row_obj.status;               
          }
          this.openSnackBar.openSnackBar('Role Details Deleted Sucessfully', 'primary');
          return true;
        });
      },
        error => {
          this.openSnackBar.openSnackBar('Error', 'danger');
          console.error(error);
        }); 
  }

}
