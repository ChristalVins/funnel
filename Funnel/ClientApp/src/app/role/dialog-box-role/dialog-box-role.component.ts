import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { RoleModel } from '../../_model/role.model';
import { GlobalConstants } from '../../_variables/global.varaible';

@Component({
  selector: 'app-dialog-box-role',
  templateUrl: './dialog-box-role.component.html',
  styleUrls: ['./dialog-box-role.component.css']
})
export class DialogBoxRoleComponent implements OnInit {
  addForm: FormGroup;  
  action:string;
  local_data:any;
  activeStatus: string[];

  constructor(
    public dialogRef: MatDialogRef<DialogBoxRoleComponent>,private fb: FormBuilder,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: RoleModel) {
    console.log(data);
    this.local_data = {...data};
    this.action = this.local_data.action;
  }
  
  ngOnInit(){
    this.activeStatus = GlobalConstants.activeStatus;
    this.addForm = this.fb.group({  
      'roleID' : [null, Validators.required],  
      'roleName' : [null, Validators.required],  
      'roles_Resp' : [null, Validators.required],
      'status' : [null, Validators.required]
    }); 

    this.addForm = this.fb.group({
      roleID:this.data.roleID ? this.data.roleID :Number,
      roleName : this.data.roleName ? this.data.roleName : '',
      roles_Resp : this.data.roles_Resp ? this.data.roles_Resp : '',
      status:this.data.status ? this.data.status :'',
    }) 
  }
 
  doAction(){
    if(this.addForm.value){
      this.dialogRef.close({event:this.action,data:this.addForm.value});
    }
  }
 
  closeDialog(){
    this.dialogRef.close({event:'Cancel'});
  }
}
