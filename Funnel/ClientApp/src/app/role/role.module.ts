import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleRouteModule } from './role.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.material.module';

import { RoleComponent } from './role.component';
import { DialogBoxRoleComponent } from './dialog-box-role/dialog-box-role.component';


@NgModule({
  declarations: [RoleComponent, DialogBoxRoleComponent],
  imports: [
    CommonModule,    
    RoleRouteModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  entryComponents: [
    DialogBoxRoleComponent
  ]

})
export class RoleModule { }
