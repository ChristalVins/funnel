﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Funnel.Model;
using Funnel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Funnel.SSO
{
    public class SsoAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        //private readonly AuthenticateService _authService;

        private const string _KeySsoUsername = "Federation_User";
        private const string _KeySsoEmailaddess = "Federation_Mail";
        private const string _KeySsoDisplayName = "Federation_DisplayName";
        private const string _BasicSchemeName = "Sso";

        public SsoAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {

        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string ssoUser = "";
            string ssoEmail = "";
            string ssoDisplayName = "";
            //UserInfoModel user = null;
            if (!Request.Headers.ContainsKey(_KeySsoUsername))
            {
                string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                if (userName.StartsWith(@"CODE1\"))
                {
                    ssoUser = userName.Substring(6);
                }
                else
                {
                    return await Task.FromResult(AuthenticateResult.Fail("TokenParseException"));
                }
            }
            else
            {
                ssoUser = Request.Headers[_KeySsoUsername].ToString();
                ssoEmail = Request.Headers[_KeySsoEmailaddess].ToString();
                ssoDisplayName = Request.Headers[_KeySsoDisplayName].ToString();
                ssoDisplayName = GetFirstName(ssoDisplayName);
                //user = await _authService.Authenticate(ssoUser, ssoEmail, ssoDisplayName);
            }

            var claims = new[] { new Claim(ClaimTypes.Name, ssoUser), new Claim(ClaimTypes.Email, ssoEmail), new Claim("DisplayName", ssoDisplayName) };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            return await Task.FromResult(AuthenticateResult.Success(ticket));
        }

        private static string GetFirstName(string ssoDisplayName)
        {
            var commaIndex = ssoDisplayName.IndexOf(',');
            if (commaIndex >= 0)
            {
                string firstName = ssoDisplayName.Substring(commaIndex + 1).Trim();
                var parenthesisIndex = firstName.IndexOf('(');
                if (parenthesisIndex >= 0)
                {
                    firstName = firstName.Substring(0, parenthesisIndex).Trim();
                }
                ssoDisplayName = firstName;
            }
            return ssoDisplayName;
        }
    }
}
