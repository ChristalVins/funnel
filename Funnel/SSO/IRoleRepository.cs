﻿using Funnel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.SSO
{
    public interface IRoleRepository
    {
        Task<UserInfoModel> GetRolesForUserAsync(string UserName);
        //Task<IEnumerable<UserInfoModel>> GetUsersForRoleAsync(string role, bool includeInactive = false);
        //Task<UserInfoModel> GetUserByIdForRoleAsync(int userId);
        //Task<IEnumerable<UserInfoModel>> GetRolesAsync();
        //Task<IEnumerable<UserInfoModel>> GetUserName(string userName, int role);
    }
}
