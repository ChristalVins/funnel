﻿using Microsoft.AspNetCore.Authentication;
using Funnel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Funnel.SSO
{
    public class CustomClaimTransformation : IClaimsTransformation
    {
        private readonly IRoleRepository _Repository;

        public CustomClaimTransformation(IRoleRepository repository)
        {
            _Repository = repository;
        }

        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            if (principal.Identity is ClaimsIdentity && principal.Identity.IsAuthenticated)
            {
                var ci = (ClaimsIdentity)principal.Identity;
                var roles = (await _Repository.GetRolesForUserAsync(principal.Identity.Name));
                if (roles !=null)
                {
                    var firstRole = roles;
                    var displayNameClaim = principal.Claims.FirstOrDefault(c => c.Type == "DisplayName");
                    if (displayNameClaim != null)
                    {
                        ci.RemoveClaim(displayNameClaim);
                    }
                    var emailClaim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                    if (emailClaim != null)
                    {
                        ci.RemoveClaim(emailClaim);
                    }
                    ci.AddClaim(new Claim("UserID", firstRole.UserID));
                    ci.AddClaim(new Claim("DisplayName", firstRole.UserName));
                    ci.AddClaim(new Claim(ClaimTypes.NameIdentifier, firstRole.UserRoleName));
                    ci.AddClaim(new Claim(ClaimTypes.Email, firstRole.UserEmailId));
                }
            }
            return principal;
        }
    }
}
