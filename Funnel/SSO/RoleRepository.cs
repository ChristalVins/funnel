﻿using Microsoft.Extensions.Configuration;
using Funnel.DataBaseContext;
using Funnel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.SSO
{
    public class RoleRepository : IRoleRepository
    {
        private ApplicationDbContext _context;
        public RoleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UserInfoModel> GetRolesForUserAsync(string UserName)
        {
            // Local
            //var user = await Task.Run(() => _context.Tbl_MPRUserInfo.FirstOrDefault(x => x.UserID == UserName && x.Status == "Active"));
            //Live
            var user = await Task.Run(() => _context.Tbl_FunnelUserInfo.FirstOrDefault(x => x.UserEmailId == UserName && x.Status == "Active"));
            
            // return null if user not found
            if (user == null)
            {
                var userId = await Task.Run(() => _context.Tbl_FunnelUserInfo.FirstOrDefault(x => x.UserID == UserName && x.Status == "Active"));
                return userId;
            }
            
            return user;
        }
    }
}
