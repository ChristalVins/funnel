﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Model
{
    public class UserInfoModel
    {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SNo { get; set; }
		[Required]
		public string UserID { get; set; }
		[Required]
		public string UserName { get; set; }
		[Required]
		public string UserRoleName { get; set; }
		[Required]
		public string UserEmailId { get; set; }
		[Required]
		public string AddedByID { get; set; }
		
		public string ModifiedBy { get; set; }
		[Required]
		public string Status { get; set; }
	}
}
