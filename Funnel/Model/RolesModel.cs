﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Model
{
    public class RolesModel
    {
		[Key]
		public int RoleID { get; set; }
		[Required]
		public string RoleName { get; set; }
		[Required]
		public string Roles_Resp { get; set; }
		[Required]
		public string Status { get; set; }
	}
}
