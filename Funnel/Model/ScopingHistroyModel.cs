﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Model
{
    public class ScopingHistroyModel
    {
        public string Employee_ID { get; set; }
        public string Stage { get; set; }
        public string Status { get; set; }
        public string File_Details { get; set; }
        public DateTime? Date { get; set; }
        public string Remarks { get; set; }
        //public string Transition_Wave_Code { get; set; }
        public string Transition_Journey_Code { get; set; }

    }
}
