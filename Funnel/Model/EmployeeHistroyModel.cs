﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.Model
{
    public class EmployeeHistroyModel
    {
        public string Employee_ID { get; set; }
        public string Location_Address_Country { get; set; }
        public string Location_Address_City { get; set; }
        public string Location { get; set; }
        public string CF_Location_Hierarchy_Geographical { get; set; }
        public string Cost_Center_ID { get; set; }
        public string Cost_Center_Name { get; set; }
        public string SO_Name { get; set; }
        public string SO_Level_Two { get; set; }
        public string SO_Level_Three { get; set; }
        public string SO_Level_Four { get; set; }
        public string Compensation_Grade { get; set; }
        public string Function_Details { get; set; }
        public string Job_Family_Group { get; set; }
        public string Job_Family { get; set; }
        public string Job_Profile { get; set; }
        public string Job_Title { get; set; }
        public string Report_Date { get; set; }
    }
}
