﻿using Microsoft.EntityFrameworkCore;
using Funnel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Funnel.DataBaseContext
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<UserInfoModel> Tbl_FunnelUserInfo { get; set; }
        public DbSet<RolesModel> Tbl_FunnelAdminRoles { get; set; }

        public DbSet<EmployeeHistroyModel> Tbl_EmployeeHistroy { get; set; }
        public DbSet<ScopingHistroyModel> Tbl_ScopingHistroy { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmployeeHistroyModel>().HasNoKey();
            modelBuilder.Entity<ScopingHistroyModel>().HasNoKey();
            //modelBuilder.Entity<EmployeeHistroyModel>()
            //.HasKey(e => new { e.Employee_ID });

            //modelBuilder.Entity<ScopingHistroyModel>()
            //.HasKey(e => new { e.Employee_ID });
        }
    }
}
